<?php $photo1 = wp_get_attachment_image_src( get_field('title_background_image'), 'title-background' ); ?>
<div data-stellar-background-ratio="0.7" data-stellar-vertical-offset="135" <?php the_field('parallax_scrolling'); ?> class="page-title-container" style="background-image: url(<?php echo $photo1[0] ?>);">
	<?php if(get_field('video_background'))
	{
		echo '<video autoplay loop muted preload="auto" class="fillWidth" src="' . get_field('video_url') . '"></video>';
	}
	?>
	<div data-stellar-ratio="0.5" class="page-title container">
		<h1 style="<?php the_field('page_title'); ?>"><?php the_title(); ?></h1>
		<div class="subheading"><?php the_field('page_subheading'); ?></div>
	</div>
	<div class="page-title-filter"></div>
</div>
<div class="container">
	<div class="row">
		<div class="project-subject col-sm-12 text-center">
			<h2><?php the_field('project_subject'); ?></h2>
		</div>
	</div>
	<div class="row">
		<?php while (have_posts()) : the_post(); ?>
		<article class="col-sm-9 col-sm-push-3 article">
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
			</footer>
			<?php comments_template('/templates/comments.php'); ?>
		</article>
		<?php endwhile; ?>
		<aside class="col-sm-3 col-sm-pull-9 post-sidebar">
			<h3 class="h4"><?php the_title(); ?></h3>
			<?php 
		        $photo = wp_get_attachment_image_src( get_field('client_logo'), 'large' );
			?>
			<img class="client-logo" src="<?php echo $photo[0] ?>">
			<p><?php the_field('client_description'); ?></p>
			<a href="<?php the_field('client_button_link'); ?>" target="_blank" class="btn btn-primary btn-block project-btn" style="<?php the_field('client_button_hide'); ?>"><?php the_field('client_button_label'); ?></a>
		</aside>
	</div>
</div>

<section class="page-section related-posts">
	<div class="related-posts-menu">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/projects/">More Projects</a>
			<a href="<?php bloginfo('url'); ?>/contact/">Connect With Keenly</a>
		</div>
	</div>
	<ul class="latest-projects">
		<?php
			$this_post = $post->ID;
		    $loop = new WP_Query( array( 'post_type' => 'projects', 'posts_per_page' => 3, 'post__not_in' => array($this_post) ) );
		    if ( $loop->have_posts() ) :
		        while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<?php
				$photo = wp_get_attachment_image_src( get_field('client_logo'), 'large' ); 
				$photo1 = wp_get_attachment_image_src( get_field('title_background_image'), 'tile-background' );
			?>
	        <li class="latest-projects-tile" style="background-image: url(<?php echo $photo1[0] ?>)">
				<div class="latest-projects-layer"></div>
				<div class="latest-projects-logo">
	                <img src="<?php echo $photo[0] ?>">
				</div>
				<div class="latest-projects-info">
					<div class="latest-projects-title-wrapper">
						<div class="latest-projects-title">
							<h3><?php the_title() ?></h3>
							<span><?php the_field('project_description') ?></span>
						</div>
					</div>
					<a href="<?php the_permalink() ?>" class="latest-projects-more">Read More</a>
				</div>
			</li>
		<?php endwhile;
        if (  $loop->max_num_pages > 1 ) : ?>
            <div id="nav-below" class="navigation">
                <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Previous', 'domain' ) ); ?></div>
                <div class="nav-next"><?php previous_posts_link( __( 'Next <span class="meta-nav">&rarr;</span>', 'domain' ) ); ?></div>
            </div>
        <?php endif;
    endif;
    wp_reset_postdata();
    ?>
	</ul>
</section>

<section class="page-section contact-section">
	<div class="container dark">
		<p class="highlight">Let's see if we like each other. It all starts with a quick note.</p>
		<a href="<?php bloginfo('url'); ?>/contact/" class="home-more h3">Contact Us <i class="fa fa-caret-square-o-right"></i></a>
	</div>
</section>