<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',  // Scripts and stylesheets
  'lib/extras.php',  // Custom functions
  'lib/setup.php',   // Theme setup
  'lib/titles.php',  // Page titles
  'lib/wrapper.php',  // Theme wrapper class
  'lib/wp_bootstrap_navwalker.php'  // Theme wrapper class
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// Add Shortcodes
function section_shortcode( $atts , $content = null ) {

  // Attributes
  extract( shortcode_atts(
    array(
      'background-color' => '',
      'text-color' => '',
      'custom-class' => '',
    ), $atts )
  );

  // Code
return '<section class="page-section ' . $atts['custom-class'] . '" style="background-color:' . $atts['background-color'] . '"><div class="container ' . $atts['text-color'] . '">' . $content . '</div></section>';
};
add_shortcode( 'section', 'section_shortcode' );

// Add Shortcodes
function section_image_shortcode( $atts , $content = null ) {

  // Attributes
  extract( shortcode_atts(
    array(
      'background-image' => '',
      'text-color' => '',
      'custom-class' => '',
    ), $atts )
  );

  // Code
return '<section class="page-section section-image ' . $atts['custom-class'] . '" style="background-image:' . $atts['background-image'] . '"><div class="container ' . $atts['text-color'] . '">' . $content . '</div></section>';
};
add_shortcode( 'section-image', 'section_image_shortcode' );


function team_shortcode( $atts ) {

  // Attributes
  extract( shortcode_atts(
    array(
      'number' => '',
      'exclude_id' => '',
      'include_id' => '',
    ), $atts )
  );

  $authors = get_users(array(
      'orderby' => 'url',
      'exclude' => $atts['exclude_id'],
      'include' => $atts['include_id'],
      'number' => $atts['number']
    )
  );
 
  $list = '';
  if($authors) : 
    $list .= '<ul class="team">';
      foreach($authors as $author) :
        $photo_id = get_user_meta($author->ID, 'profile_photo', true);
        $photo = wp_get_attachment_image_src( $photo_id, 'large' );
        $archive_url = get_author_posts_url($author->ID);
        $link = esc_url(home_url('/'));

        $list .= '<li class="team-tile" style="background-image: url(' . $photo[0] . ')">';
          $list .= '<div class="team-layer"></div>';
          $list .= '<div class="team-title"><p class="name">' . $author->display_name . '</p><p class="position">' . get_user_meta($author->ID, 'position', true) . '</p></div>';
          $list .= '<div class="team-info"><div class="team-bio"><span>' . $author->description . '</span><a href="' . $link . 'contact?teamid=' . get_user_meta($author->ID, 'first_name', true) . '" class="team-message">Send ' . get_user_meta($author->ID, 'first_name', true) . ' A Message</a></div></div>';
        $list .= '</li>';
      endforeach;
    $list .= '</ul>';
  endif;
 
  return $list;
}
add_shortcode('team', 'team_shortcode');

function projects_shortcode() {
    $output = '<ul class="latest-projects">';
    $args = array(
        'post_type' => 'projects',
        'posts_per_page' => 3,
    );
    $projects_query = new  WP_Query( $args );
    while ( $projects_query->have_posts() ) : $projects_query->the_post();
        $photo = wp_get_attachment_image_src( get_field('client_logo'), 'large' );
        $image = wp_get_attachment_image_src( get_field('title_background_image'), 'tile-background' );
        $output .= '<li class="latest-projects-tile" style="background-image: url('.$image[0].')">
                      <div class="latest-projects-layer"></div>
                      <div class="latest-projects-logo">
                        <img src="'.$photo[0].'">
                      </div>
                      <div class="latest-projects-info">
                        <div class="latest-projects-title-wrapper">
                          <div class="latest-projects-title">
                            <h3>'.get_the_title().'</h3>
                            <span>'.get_field('project_description').'</span>
                          </div>
                        </div>
                        <a href="'.get_permalink().'" class="latest-projects-more">Read More</a>
                      </div>
                    </li>';
    endwhile;
    wp_reset_query();
    $output .= '</ul>';
    return $output;
}
add_shortcode('projects', 'projects_shortcode');

function profile_photo( $avatar, $id_or_email, $size, $default, $alt ) {
    // Get user by id or email
    if ( is_numeric( $id_or_email ) ) {
        $id   = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );
    } elseif ( is_object( $id_or_email ) ) {
        if ( ! empty( $id_or_email->user_id ) ) {
            $id   = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }
    } else {
        $user = get_user_by( 'email', $id_or_email );
    }
    // Get the user id
    $user_id = $user->ID;
    // Get the file id
    $image_id = get_user_meta($user_id, 'profile_photo', true); // CHANGE TO YOUR FIELD NAME
    // Bail if we don't have a local avatar
    if ( ! $image_id ) {
        return $avatar;
    }
    // Get the file size
    $image_url  = wp_get_attachment_image_src( $image_id, 'thumbnail' ); // Set image size by name
    // Get the file url
    $avatar_url = $image_url[0];
    // Get the img markup
    $avatar = '<img alt="' . $alt . '" src="' . $avatar_url . '" class="avatar avatar-' . $size . '" height="' . $size . '" width="' . $size . '"/>';
    // Return our new avatar
    return $avatar;
}
add_filter('get_avatar', 'profile_photo', 10, 5);

// Projects Posts Type
function project_post() {

  $labels = array(
    'name'               => _x( 'Projects', 'post type general name' ),
    'singular_name'      => _x( 'Project', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'project' ),
    'add_new_item'       => __( 'Add New Project' ),
    'edit_item'          => __( 'Edit Project' ),
    'new_item'           => __( 'New Project' ),
    'all_items'          => __( 'All Projects' ),
    'view_item'          => __( 'View Project' ),
    'search_items'       => __( 'Search Projects' ),
    'not_found'          => __( 'No projects found' ),
    'not_found_in_trash' => __( 'No projects found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Projects'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our projects and project specific data',
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'revisions', 'excerpt' ),
    'has_archive'   => true,
  );

  register_post_type( 'projects', $args ); 

}
add_action( 'init', 'project_post' );

// Resources Posts Type
function resource_post() {

  $labels = array(
    'name'               => _x( 'Resources', 'post type general name' ),
    'singular_name'      => _x( 'Resource', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'resource' ),
    'add_new_item'       => __( 'Add New Resource' ),
    'edit_item'          => __( 'Edit Resource' ),
    'new_item'           => __( 'New Resource' ),
    'all_items'          => __( 'All Resources' ),
    'view_item'          => __( 'View Resource' ),
    'search_items'       => __( 'Search Resources' ),
    'not_found'          => __( 'No resource found' ),
    'not_found_in_trash' => __( 'No resource found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Resources'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our resources and resource specific data',
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 6,
    'supports'      => array( 'title', 'thumbnail', 'editor', 'revisions', 'excerpt', 'comments', 'author' ),
    'has_archive'   => true,
    'taxonomies' => array('category', 'post_tag'),
  );

  register_post_type( 'resources', $args ); 

}
add_action( 'init', 'resource_post' );


add_filter('the_content','my_custom_formatting');

function my_custom_formatting($content){
if(get_post_type()=='page')
    return $content;
else
 return wpautop($content);
}
remove_filter( 'the_content', 'wpautop' );

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

// Custom Image Sizes
add_image_size( 'tile-background', 640, 510, true );
add_image_size( 'title-background', 1920, 1080, true );
add_image_size( 'feature-list', 750, 270, true );

function remove_menu_pages() {
    remove_menu_page('edit.php');
}

add_action( 'admin_menu', 'remove_menu_pages' );

function wp_bs_pagination($pages = '', $range = 4)
  {
   $showitems = ($range * 2) + 1;
     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '')
     {
         global $wp_query;
     $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }
     if(1 != $pages)
     {
        echo '<div class="text-center">';
        echo '<nav><ul class="pagination"><li class="disabled hidden-xs"><span><span aria-hidden="true">Page '.$paged.' of '.$pages.'</span></span></li>';
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."' aria-label='First'>&laquo;<span class='hidden-xs'> First</span></a></li>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."' aria-label='Previous'>&lsaquo;<span class='hidden-xs'> Previous</span></a></li>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class=\"active\"><span>".$i." <span class=\"sr-only\">(current)</span></span>
    </li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
             }
         }
         if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\"  aria-label='Next'><span class='hidden-xs'>Next </span>&rsaquo;</a></li>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."' aria-label='Last'><span class='hidden-xs'>Last </span>&raquo;</a></li>";
         echo "</ul></nav>";
         echo "</div>";
     }
}

include_once 'forms/self-assessment.php';

function chart_script() {
  wp_register_script('chart-script', get_template_directory_uri() . '/assets/scripts/Chart.min.js');
  
  if ( is_page( 'self-assessment' )) {
    wp_enqueue_script('chart-script');
  }
}
add_action( 'wp_enqueue_scripts', 'chart_script', 100 );