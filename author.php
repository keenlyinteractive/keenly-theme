<?php 
			        $bkphoto = wp_get_attachment_image_src( get_the_author_meta('banner'), 'full' );
				?>
<div data-stellar-background-ratio="0.7" data-stellar-vertical-offset="135" class="page-title-container" style="background-image: url(<?php echo $bkphoto[0] ?>);">
	<video src="<?php the_author_meta('video'); ?>" class="fillWidth" preload="auto" muted="" loop="" autoplay=""></video>	
	<div data-stellar-ratio="0.5" class="page-title container">
		<h1 style="<?php the_field('page_title'); ?>">Meet <?php the_author(); ?></h1>
		<div class="subheading"><?php the_author_meta('sub_section'); ?></div>
	</div>
	<div class="page-title-filter"></div>
</div>

<div class="container" style="margin: 50px auto;">
	<div class="row">
		<aside class="col-sm-4">
			<div class="post-sidebar">
				<?php 
			        $photo = wp_get_attachment_image_src( get_the_author_meta('profile_photo'), 'thumbnail' );
				?>
				<img class="profile-photo" src="<?php echo $photo[0] ?>">
				<h1 class="entry-title h4"><?php the_author(); ?></h1>
				<p class="author-position"><?php the_author_meta('position'); ?></p>
				<p><?php the_author_meta('description'); ?></p>
			</div>
		</aside><!-- /.sidebar -->

		<main class="col-sm-8">
			<div class="author-article-list">
				<?php if (!have_posts()) : ?>
				  <div class="alert alert-warning">
				    <?php _e('Sorry, no results were found.', 'sage'); ?>
				  </div>
				  <?php get_search_form(); ?>
				<?php endif; ?>

				<?php while (have_posts()) : the_post(); ?>
					<article class="author-article">
						<?php
						$image = wp_get_attachment_image( get_post_thumbnail_id($post->ID), 'feature-list' );
						?>
						<?php echo $image ?>
						<h2 class="entry-title h3"><?php the_title(); ?></h2>
						<div class="author-article-content">
							<div><time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time></div>
							<p><?php the_excerpt(); ?></p>
							<a href="<?php echo get_site_url(); ?>/resources/<?php echo $post->post_name; ?>" class="btn btn-primary">read more</a>
						</div>
						<div class="clearfix"></div>
					</article>
				<?php endwhile; ?>

				<?php the_posts_navigation(); ?>
			</div>
		</main>
	</div>
</div>