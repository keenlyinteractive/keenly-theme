<div data-stellar-background-ratio="0.7" data-stellar-vertical-offset="135" class="page-title-container" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/images/banner.jpg);">
	<video src="http://localhost:8888/keenly/wp-content/uploads/2016/04/keen_home_loop.mp4" class="fillWidth" preload="auto" muted="" loop="" autoplay=""></video>	
	
	<div data-stellar-ratio="0.5" class="page-title container">
		<h1 style="<?php the_field('page_title'); ?>"><?php post_type_archive_title(); ?></h1>
		<div class="subheading">We love working on things that help others</div>
	</div>
	<div class="page-title-filter"></div>
</div>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<ul class="latest-projects">
	<?php while (have_posts()) : the_post(); ?>
		<?php 
			$photo = wp_get_attachment_image_src( get_field('client_logo'), 'large' );
			$photo1 = wp_get_attachment_image_src( get_field('title_background_image'), 'tile-background' );
		?>
        <li class="latest-projects-tile" style="background-image: url(<?php echo $photo1[0] ?>)">
			<div class="latest-projects-layer"></div>
			<div class="latest-projects-logo">
                <img src="<?php echo $photo[0] ?>">
			</div>
			<div class="latest-projects-info">
				<div class="latest-projects-title-wrapper">
					<div class="latest-projects-title">
						<h3><?php the_title() ?></h3>
						<span><?php the_field('project_description') ?></span>
					</div>
				</div>
				<a href="<?php the_permalink() ?>" class="latest-projects-more">Read More</a>
			</div>
		</li>
	<?php endwhile; ?>
</ul>

<div class="post-navigation"> 
	<div class="container">
		<?php the_posts_navigation(); ?>
	</div>
</div>

<section class="page-section contact-section">
	<div class="container dark">
		<p class="highlight">Maybe we could change the world together. It all starts with a quick note.</p>
		<a href="<?php bloginfo('url'); ?>/contact/" class="home-more h3">Contact Us <i class="fa fa-caret-square-o-right"></i></a>
	</div>
</section>