<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="bars"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"></a>
    </div>

    <nav class=" menu" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
      endif;
      ?>
    </nav>
  </div>
</header>
<div class="header-spacing"></div>