<?php 
	$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'title-background' );
?>
<div <?php the_field('parallax_scrolling'); ?> class="page-title-container" style="background-image: url(<?php echo $image[0] ?>);">
	<?php if(get_field('video_background'))
	{
		echo '<video autoplay loop muted preload="auto" class="fillWidth" src="' . get_field('video_url') . '"></video>';
	}
	?>
	<div data-stellar-ratio="0.5" class="page-title container">
		<h1 style="<?php the_field('page_title'); ?>"><?php the_title(); ?></h1>
		<div class="subheading"><?php the_field('page_subheading'); ?></div>
	</div>
	<div class="page-title-filter"></div>
</div>
<div class="container resource">
	<div class="row">
		<?php while (have_posts()) : the_post(); ?>
		<article class="col-sm-9 col-sm-push-3 article">
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
			</footer>
			<?php comments_template('/templates/comments.php'); ?>
		</article>
		<?php endwhile; ?>
		<aside class="col-sm-3 col-sm-pull-9 post-sidebar">
			<?php 
		        $photo = wp_get_attachment_image_src( get_the_author_meta('profile_photo'), 'thumbnail' );
			?>
			<img class="profile-photo" src="<?php echo $photo[0] ?>">
			<h3 class="h4 author-name"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></h3>
			<p class="author-position"><?php the_author_meta('position'); ?></p>
			<p><?php the_author_meta('description'); ?></p>
		</aside>
	</div>
</div>

<section class="page-section related-posts">
	<div class="related-posts-menu">
		<div class="container">
			<a href="<?php bloginfo('url'); ?>/resources/">More Resources</a>
			<a href="<?php bloginfo('url'); ?>/contact/">Connect With Keenly</a>
		</div>
	</div>
	<ul class="post-archive">
		<?php
			$this_post = $post->ID;
		    $loop = new WP_Query( array( 'post_type' => 'resources', 'posts_per_page' => 3, 'post__not_in' => array($this_post) ) );
		    if ( $loop->have_posts() ) :
		        while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<?php 
				$image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
				$category = get_the_category();
			?>
			<li class="post-archive-tile <?php echo $category[0]->cat_name; ?>" style="background-image: url(<?php echo $image_url; ?>)">
				<div class="post-archive-layer"></div>
				<div class="post-archive-title">
					<h2><?php the_title(); ?></h2>
				</div>
				<div class="post-archive-info">
					<div class="post-archive-info-title-wrapper">
						<div class="post-archive-info-title">
							<h3><?php echo $category[0]->cat_name; ?></h3>
							<p class="excerpt"><?php echo excerpt(18); ?></p>
						</div>
					</div>
					<a href="<?php the_permalink() ?>" class="post-archive-more">Read More</a>
				</div>
			</li>
		<?php endwhile;
        if (  $loop->max_num_pages > 1 ) : ?>
            <div id="nav-below" class="navigation">
                <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Previous', 'domain' ) ); ?></div>
                <div class="nav-next"><?php previous_posts_link( __( 'Next <span class="meta-nav">&rarr;</span>', 'domain' ) ); ?></div>
            </div>
        <?php endif;
    endif;
    wp_reset_postdata();
    ?>
	</ul>
</section>

<section class="page-section contact-section">
	<div class="container dark">
		<p class="highlight">Let's see if we like each other. It all starts with a quick note.</p>
		<a href="<?php bloginfo('url'); ?>/contact/" class="home-more h3">Contact Us <i class="fa fa-caret-square-o-right"></i></a>
	</div>
</section>

<?php the_field('video_background'); ?>