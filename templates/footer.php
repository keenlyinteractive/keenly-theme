<footer class="content-info">
  <div class="container">
  	<a href="<?= esc_url(home_url('/')); ?>" class="footer-logo"></a>
  	<p class="footer-text">
	  	<span class="footer-tagline">reach more. help more. do more.</span>
	  	<span class="footer-copyrights">&copy; <?php echo date("Y") ?> Keenly Interactive. All Rights Reserved.</span>
  	</p>
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
