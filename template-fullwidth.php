<?php
/**
 * Template Name: Fullwidth Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<div <?php the_field('parallax_scrolling'); ?> class="page-title-container" style="background-image: url(<?php the_field('title_background_image'); ?>);">
		<div data-stellar-ratio="0.5" class="page-title container">
			<h1 style="<?php the_field('page_title'); ?>"><?php the_title(); ?></h1>
			<div class="subheading"><?php the_field('page_subheading'); ?></div>
		</div>
		<div class="page-title-filter"></div>
	</div>
	<div class="container page-content">
		<?php get_template_part('templates/content', 'page'); ?>
	</div>
<?php endwhile; ?>