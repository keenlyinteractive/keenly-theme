<?php
/**
 * Template Name: Client Form Template
 */
?>

<style>
	header {display:none;}
	footer {display: none;}
	
</style>	

<?php while (have_posts()) : the_post(); ?>

	<div class="container page-content" style="padding:20px;">
	<!--h3 style="<?php the_field('page_title'); ?>"><?php the_title(); ?></h3-->
		<?php get_template_part('templates/content', 'page'); ?>
	</div>
<?php endwhile; ?>