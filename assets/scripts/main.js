/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

  $('.page-title').delay(0).queue(function(){
    $(this).addClass("load");
  });

  $( function() {
    // init Isotope
    var $grid = $('.post-archive').isotope({
      itemSelector: '.post-archive-tile',
      layoutMode: 'fitRows'
    });
    // filter functions
    var filterFns = {};
    // bind filter button click
    $('.filters-button-group').on( 'click', 'button', function() {
      var filterValue = $( this ).attr('data-filter');
      // use filterFn if matches value
      filterValue = filterFns[ filterValue ] || filterValue;
      $grid.isotope({ filter: filterValue });
    });
    // change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
      var $buttonGroup = $( buttonGroup );
      $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $( this ).addClass('is-checked');
      });
    });
    
  });

  if($('.sub-menu-spacing').length >0 ){
    var $window = $(window),
       $stickyEl = $('.sub-menu-spacing'),
       elTop = $stickyEl.offset().top -86;

   $window.scroll(function() {
        $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop);
    });
  }

  $(".navbar-toggle").on('click', function(){
    $(this).toggleClass('active');
  });

  $(".navbar-toggle").on('click', function(){
    $('.menu').toggleClass('active');
  });

  $(".navbar-toggle").on('click', function(){
    $('body').toggleClass('active');
  });

  $( function() {
    var $window = $(window),
            $html = $('.menu');
        function resize() {
            if ($window.width() < 768) {
                return $html.addClass('transition');
            }

            $html.removeClass('transition');
        }
        $window
            .resize(resize)
            .trigger('resize');
  });

  $( function() {
    var $window = $(window);
        function resize() {
            if ($window.width() < 768) {
                
            }
        }
        $window
            .resize(resize)
            .trigger('resize');
  });

  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
  };

  $(function(){
    if( !isMobile.any() ){
        $.stellar({ horizontalScrolling: false, positionProperty: 'transform', verticalOffset: -20 });
    }
    else {
      $('video').remove();
    }
  });

  $(function(){
    if(navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {     
        $(".post-archive-tile").on('click', function(){
          $('.post-archive-tile.active').removeClass('active');
          $(this).closest('.post-archive-tile').addClass('active');
        });    
    }
    else
    {}
  });

})(jQuery); // Fully reference jQuery after this point.