<?php add_filter("gform_pre_submission_7", "wdm_evaluate_results");

function wdm_evaluate_results($form)
{
    $Summarytotal = 0;
    $Atotal = 0;
    $Btotal = 0;
    $Ctotal = 0;
    $Dtotal = 0;
    $Etotal = 0;


//Clarity Of Vision
    // add the values of selected results
    $Atotal += $_POST["input_1"] ;
    $Atotal += $_POST["input_2"] ;
    $Atotal += $_POST["input_3"] ;
    $Atotal += $_POST["input_4"] ;
    $Atotal += $_POST["input_5"];

    // set the value of the hidden field
    $_POST["input_6"] = $Atotal;


//Play Well with Others
    // add the values of selected results
    $Btotal += $_POST["input_8"] ;
    $Btotal += $_POST["input_9"] ;
    $Btotal += $_POST["input_10"] ;
    $Btotal += $_POST["input_11"] ;
    $Btotal += $_POST["input_12"];

    // set the value of the hidden field
    $_POST["input_13"] = $Btotal;


// Knowledge of Audience
    // add the values of selected results
    $Ctotal += $_POST["input_15"] ;
    $Ctotal += $_POST["input_16"] ;
    $Ctotal += $_POST["input_17"] ;
    $Ctotal += $_POST["input_18"] ;
    $Ctotal += $_POST["input_19"];

    // set the value of the hidden field
    $_POST["input_20"] = $Ctotal;  

//Adapability 
    // add the values of selected results
    $Dtotal += $_POST["input_22"] ;
    $Dtotal += $_POST["input_23"] ;
    $Dtotal += $_POST["input_24"] ;
    $Dtotal += $_POST["input_25"] ;
    $Dtotal += $_POST["input_26"];

    // set the value of the hidden field
    $_POST["input_27"] = $Dtotal;    


// Efficacy
    // add the values of selected results
    $Etotal += $_POST["input_29"] ;
    $Etotal += $_POST["input_30"] ;
    $Etotal += $_POST["input_31"] ;
    $Etotal += $_POST["input_32"] ;
    $Etotal += $_POST["input_33"];

    // set the value of the hidden field
    $_POST["input_34"] = $Etotal;

//Total Summary
    // add the values of selected results
    $Summarytotal += $_POST["input_6"] ;
    $Summarytotal += $_POST["input_13"] ;
    $Summarytotal += $_POST["input_20"] ;
    $Summarytotal += $_POST["input_27"] ;
    $Summarytotal += $_POST["input_34"];

    // set the value of the hidden field
    $_POST["input_72"] = $Summarytotal;





/*function between($x, $lim1, $lim2) {
  if ($lim1 < $lim2) {
    $lower = $lim1; $upper = $lim2;
  }
  else {
    $lower = $lim2; $upper = $lim1;
  }
  return (($x >= $lower) && ($x <= $upper));
}*/

$Result1Title ="0-20: CODE RED";
$Result1 ="It's possible you skipped some questions... or you're on the verge of total burnout. At this point, you're feeling ill-equipped and isolated, you don't have any collaborative relationships with other organizations and it's hardly worth picking up the phone to reach donors. You're in need of serious organizational (and self) assessment to see how - and if - you want to continue. A neutral third party may help lend an objective perspective. That's where we come in.";

$Result2Title ="21-40 THE STRUGGLE IS REAL";
$Result2 ="You're frustrated, unsure of your next steps and your organization is potentially stuck in its ways. It’s possible that you’re a young organization, and you’re not sure how to get past survival mode. It could also be that your team has fallen into habits over the years that are difficult to change, doing things the old way because that's how it's always been done. You're in need of clear direction, dramatically improved communication, and a renewed awareness of where you're going and how you fit into that future. It's possible that you lack the mutual benefits that come from connection to other non-profits and a clear understanding of who your audience is and who you're serving, as well as a way to measure your effectiveness.  A neutral third party may help lend an objective perspective. That's where we come in.";

$Result3Title ="41-60 THE COMPROMISER";
$Result3 ="You've got a can-do attitude and a solid understanding of what your organization is all about. You're working hard and getting some results, but would benefit from improved communication (both external and internal) and not letting tradition hamper your progress. There may be some legacy projects and programs that, because of relationships and politics, are continuing despite limited results. In terms of enthusiasm for your cause, some stagnation has set in, and no one is ultimately responsible to a hope or a vision. You're maintaining organizational infrastructure at the expense of networking potential, leaving opportunity on the table. A neutral third party may help lend an objective perspective. That's where we come in.";

$Result4Title ="61-80 THE EFFECTIVE ISOLATIONIST";
$Result4 ="Your organization is functioning pretty well, with clarity of vision and solid communication. You're ready to work with other organizations, but you tend to always want to be the headliner. There is value to be found in partnering to share leadership or letting someone else take the lead. It's important to prioritize doing the most good over getting the most recognition. A neutral third party may help lend an objective perspective. That's where we come in.";

$Result5Title ="81-100 THE SUCCESS STORY";
$Result5 ="You're fully engaged in partnership. You see the big picture, but you're good at the details, too. Your organization is healthy and maintains a clear vision of your cause, your donors, and those you help. It appears that you're an excellent example for other organizations to follow.";


//if between($Summarytotal, 21, 40) :: $_POST["input_78"] = $SummaryWords1;
//endif;

if ($Summarytotal >= 0 && $Summarytotal <= 20 ){
$SummaryEntry = $Result1;
$SummaryTitle = $Result1Title;
}
elseif ($Summarytotal >= 21 && $Summarytotal <= 40){
$SummaryEntry = $Result2;
$SummaryTitle = $Result2Title;
}
elseif ($Summarytotal >= 41 && $Summarytotal <= 60){
$SummaryEntry = $Result3;
$SummaryTitle = $Result3Title;
}
elseif ($Summarytotal >= 61 && $Summarytotal <= 80){
$SummaryEntry = $Result4;
$SummaryTitle = $Result4Title;
}
elseif ($Summarytotal >= 81 && $Summarytotal <= 100){
$SummaryEntry = $Result5;
$SummaryTitle = $Result5Title;
}

$_POST["input_78"] = $SummaryEntry;
$_POST["input_79"] = $SummaryTitle;


}





