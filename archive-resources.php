<div data-stellar-background-ratio="0.7" data-stellar-vertical-offset="135" class="page-title-container" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/images/banner.jpg);">
	<video src="<?php echo get_template_directory_uri(); ?>/dist/images/keen_home_loop.mp4" class="fillWidth" preload="auto" muted="" loop="" autoplay=""></video>	
	<div data-stellar-ratio="0.5" class="page-title container">
		<h1 style="<?php the_field('page_title'); ?>"><?php post_type_archive_title(); ?></h1>
		<div class="subheading">Ideas, tips, articles and more</div>
	</div>
	<div class="page-title-filter"></div>
</div>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<div class="sub-menu-spacing">
	<div class="sub-menu">
		<div class="button-group filters-button-group container">
		  <button class="button is-checked" data-filter="*">All</button>
		  <button class="button" data-filter=".Ideas">Ideas</button>
		  <button class="button" data-filter=".Market">Market</button>
		</div>
	</div>
</div>

<ul class="post-archive">
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	<?php endwhile; ?>
</ul>

<div class="post-navigation"> 
	<div class="container">
		<?php
		if (function_exists("wp_bs_pagination"))
		  {
		    wp_bs_pagination();
		  }
		?>
	</div>
</div>

<section class="page-section contact-section">
	<div class="container dark">
		<p class="highlight">Maybe we could change the world together. It all starts with a quick note.</p>
		<a href="<?php bloginfo('url'); ?>/contact/" class="home-more h3">Contact Us <i class="fa fa-caret-square-o-right"></i></a>
	</div>
</section>