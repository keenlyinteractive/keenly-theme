<div data-smooth-scrolling="off" data-0="background-position:center 0px;" data-10000="background-position:center 3000px;" class="page-title-container" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/images/banner.jpg);">
	<div data-smooth-scrolling="off" data-0="transform:translateY(-50%);" data-1000p="transform:translateY(3000%);" class="page-title container">
		<h1 style="<?php the_field('page_title'); ?>"><?php post_type_archive_title(); ?></h1>
		<div class="subheading">Some things you can use</div>
	</div>
	<div class="page-title-filter"></div>
</div>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<ul class="post-archive">
	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	<?php endwhile; ?>
</ul>

<div class="post-navigation"> 
	<div class="container">
		<?php the_posts_navigation(); ?>
	</div>
</div>