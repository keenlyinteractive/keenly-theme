<?php 
	$image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'tile-background' );
	$category = get_the_category();
?>
<li class="post-archive-tile <?php echo $category[0]->cat_name; ?>" style="background-image: url(<?php echo $image_url[0] ?>)">
	<div class="post-archive-layer"></div>
	<div class="post-archive-title">
		<h2><?php the_title(); ?></h2>
	</div>
	<div class="post-archive-info">
		<div class="post-archive-info-title-wrapper">
			<div class="post-archive-info-title">
				<h3><?php echo $category[0]->cat_name; ?></h3>
				<p class="excerpt"><?php echo excerpt(18); ?></p>
			</div>
		</div>
		<a href="<?php the_permalink() ?>" class="post-archive-more">Read More</a>
	</div>
</li>