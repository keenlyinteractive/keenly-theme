<?php
/**
 * Template Name: Fullwidth (No Container)
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<div <?php the_field('parallax_scrolling'); ?> class="page-title-container" style="background-image: url(<?php the_field('title_background_image'); ?>);">
		<?php if(get_field('video_background'))
		{
			echo '<video autoplay loop muted preload="auto" class="fillWidth" src="' . get_field('video_url') . '"></video>';
		}
		?>
		<div data-stellar-ratio="0.5" class="page-title container">
			<h1 style="<?php the_field('page_title'); ?>"><?php the_title(); ?></h1>
			<div class="subheading"><?php the_field('page_subheading'); ?></div>
		</div>
		<div class="page-title-filter"></div>
	</div>
	<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>